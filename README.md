# tmdb-app
An app built with ReactJS that fetch some datas about tv shows and movies. Study purposes only.

## Dependencies
This application depends on [fetch](https://fetch.spec.whatwg.org/) to make requests to the [TMDb API](https://developers.themoviedb.org/3). For environments that don't support fetch, you'll need to provide a [polyfill](https://github.com/github/fetch) to browser or [polyfill](https://github.com/bitinn/node-fetch) to Node.

## How to Run

1. Run git clone
  - ssh: git clone `git@github.com:lucasmls/tmdb-app.git`
  - https: git clone `https://github.com/lucasmls/tmdb-app.git`
2. Install the dependencies with `npm install`.
3. Run your application with `npm start`.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
