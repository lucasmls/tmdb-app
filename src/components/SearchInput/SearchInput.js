import React from 'react'
import PropTypes from 'prop-types'
import './SearchInput.css'

const SearchInput = ({ handleSearch }) => (
  <input className='search' placeholder='Search for a movie or tv show' type='text' onKeyUp={handleSearch} />
)

SearchInput.propTypes = {
  handleSearch: PropTypes.func.isRequired
}

export default SearchInput
