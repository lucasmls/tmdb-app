import React from 'react'
import spinner from './spin.svg'

const Spinner = () => (
  <div style={{textAlign: 'center'}}>
    <img src={spinner} style={{width: '50px', height: '50px'}} />
  </div>
)

export default Spinner
