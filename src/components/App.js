import React, { Component } from 'react'
import SearchInput from './SearchInput/SearchInput'
import MovieList from './MovieList/MovieList'
import Spinner from './Spinner/Spinner'
import './app.css'

class App extends Component {
  constructor () {
    super()
    this.state = {
      tvs: [],
      movies: [],
      isFetching: false
    }

    this.handleSearch = this.handleSearch.bind(this)
  }

  handleSearch (e) {
    const value = e.target.value.split(' ')
    const keyCode = e.keyCode
    const ENTER = 13
    const API_KEY = '4be6bb7625e0a14c4c3abeeb525b2209'

    if (keyCode === ENTER) {
      this.setState({
        tvs: [],
        movies: [],
        isFetching: true
      })
    }

    if (keyCode === ENTER) {
      fetch(`https://api.themoviedb.org/3/search/multi?api_key=${API_KEY}&language=pt-BRS&query=${value.join('+')}`)
        .then(res => res.json())
        .then(res => this.setState({
          tvs: res.results.filter(tv => tv.media_type === 'tv'),
          movies: res.results.filter(movie => movie.media_type === 'movie')
        }))
        .then(res => this.setState({isFetching: false}))
    }
  }

  render () {
    return (
      <div className='app'>
        <SearchInput style={{display: 'inline-block'}}
          handleSearch={this.handleSearch}
        />

        {this.state.isFetching && <Spinner />}

        <div className='container'>
          <MovieList
            data={this.state.movies}
            title='Movies'
          />

          <MovieList
            data={this.state.tvs}
            title='TV Shows'
          />
        </div>
      </div>
    )
  }
}

export default App
