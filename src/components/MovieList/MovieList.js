import React from 'react'
import PropTypes from 'prop-types'
import './MovieList.css'

function imageExists (url) {
  return url !== 'https://image.tmdb.org/t/p/w500null' ? url : 'http://www.ateneo.edu/sites/default/files/inserted/No_image_available.jpg'
}

function checkDate (date) {
  return date == null ? 'Date were not provided' : date
}

function convertToBRDate (date) {
  const check = checkDate(date)
  return check === 'Date were not provided' ? check : `Release date: ${date.split('-').reverse().join('/')}`
}

const MovieList = ({ data, title }) => (
  <div className='list'>
    <h1>{title}</h1>
    {data.map((data, index) => {
      return (
        <div key={index} className='item'>
          <img src={imageExists(`https://image.tmdb.org/t/p/w500${data.poster_path}`)} />
          <div className='infos'>
            <h2>{data.original_title}</h2>
            <p>{data.overview}</p>
            <span>{convertToBRDate(data.release_date)}</span>
          </div>
        </div>
      )
    })}
  </div>
)

MovieList.propTypes = {
  data: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired
}

export default MovieList
